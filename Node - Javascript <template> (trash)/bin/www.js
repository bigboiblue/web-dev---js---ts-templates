const app = require("../app");

//// Any Startup Scripts Here ////

const PORT = process.env.port || 8000;
app.listen(PORT, () => console.log(`Server running on port: ${PORT}`));

//// Server error handling here ////
//// e.g.  app.on('error', onError); ////