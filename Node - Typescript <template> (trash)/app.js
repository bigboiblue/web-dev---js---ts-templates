"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

const express = require("express");

const app = express();
app.use(require("./routes"));
app.use(express.static("./public"));
module.exports = app;
