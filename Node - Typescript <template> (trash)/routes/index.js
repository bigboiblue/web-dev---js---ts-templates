"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

const express = require("express");

const router = express.Router();
router.use("/api", require("./api"));
module.exports = express;
