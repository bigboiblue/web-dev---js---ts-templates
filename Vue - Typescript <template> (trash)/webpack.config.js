const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
	entry: {
		app: path.resolve(__dirname, "src/main.ts"),
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		// publicPath: '/assets/', // When hostname/assets/ is accessed from a browser,
		// it is rerouted to the dist folder.
		filename: "[name]_bundle.js",
	},
	module: {
		rules: [
			// Vue to js
			{
				test: /\.vue$/,
				loader: "vue-loader",
				options: {
					loaders: {
						scss: "vue-style-loader!css-loader!sass-loader",
						sass: "vue-style-loader!css-loader!sass-loader?indentedSyntax",
					},
				},
			},
			// Scss to css, then inject into html
			{
				test: /\.s?css$/,
				loader: ["vue-style-loader", "css-loader", "sass-loader"],

			},
			// Ts to js
			{
				test: /\.tsx?$/,
				loader: "ts-loader",
				exclude: "/node_modules/",
				options: {
					appendTsSuffixTo: [/\.vue$/], //Parses vue imports as typescript
				},
			},
			// General image file loader
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: "file-loader",
				options: {
					name: "[name].[ext]?[hash]",
				},
			},
		],
	},
	devtool: "cheap-module-source-map",
	performance: {
		hints: false,
	},
	devServer: {
		noInfo: true, //Suppress unneeded console info
		historyApiFallback: true, // Use HTML5 History API for routing
		hot: true, // Retain web app state on source code change
	},
	resolve: {
		extensions: [".ts", ".js", ".vue", ".json"],
		alias: {
			vue$: "vue/dist/vue.esm.js", // Separates vue framework as external es6 import file
		},
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, "src/index.html"),
		}),
		new VueLoaderPlugin(),
	],

};

// Production Mode Optimisations
if (process.env.NODE_ENV === "production") {
	module.exports.devtool = "#source-map";
	// http://vue-loader.vuejs.org/en/workflow/production.html
	module.exports.plugins = (module.exports.plugins || []).concat([
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: "\"production\"",
			},
		}),
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true,
			compress: {
				warnings: false,
			},
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
		}),
	]);
}
