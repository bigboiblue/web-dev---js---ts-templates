import Vue from "vue";
import App from "./App.vue";

const myApp = new Vue({
	el: "#app",
	render: h => h(App),
});
